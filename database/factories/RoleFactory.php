<?php

namespace Backtheweb\Roles\Database\Factories;

use Backtheweb\Roles\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleFactory extends Factory
{
    protected $model = Role::class;

    public function definition()
    {
        return [
            'name' => $this->faker->words(1, true),
        ];
    }
}
