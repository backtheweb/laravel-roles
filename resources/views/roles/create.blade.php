<x-app-layout>
    <x-slot name="header">
        <a class="btn-primary float-right" href="{{ route('roles.create') }}">{{ __("roles::messages.create") }}</a>
        <h2 class="font-semibold text-xl text-gray-800 leading-tight"><a href="{{ route('roles.index') }}">{{ __('roles::messages.title') }}</a></h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 flex flex-col space-y-4">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div class="p-6 bg-white border-b border-gray-200 flex flex-wrap justify-between items-center">
                    <form method="POST" action="{{ route('roles.store') }}">
                    @csrf
                        <!-- Email Address -->
                        <div class="form-group">
                            <x-label for="email" :value="__('roles::messages.name')" />
                            <x-input id="email"
                                     class="w-full" type="text"
                                     name="name"
                                     :value="old('name')"
                                     required autofocus />

                            @error('name')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror

                        </div>
                        <x-button type="submit">{{__("roles::messages.save")}}</x-button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
