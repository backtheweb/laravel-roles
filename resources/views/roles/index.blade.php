<x-app-layout>
    <x-slot name="header">
        <a class="btn-primary float-right" href="{{ route('roles.create') }}">{{ __("roles::messages.create") }}</a>
        <h2 class="font-semibold text-xl text-gray-800 leading-tight"><a href="{{ route('roles.index') }}">{{ __('roles::messages.title') }}</a></h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 flex flex-col space-y-4">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                @forelse ($roles as $role)
                    <div class="p-6 bg-white border-b border-gray-200 flex flex-wrap justify-between items-center">
                        <span>{{ $role->name }}</span>
                        <div class="flex space-x-4 items-center">
                            <a href="{{ route("roles.show",    $role) }}"><i class="fa-solid fa-fw fa-eye"></i></a>
                            <a href="{{ route("roles.edit",    $role) }}"><i class="fa-solid fa-fw fa-pencil"></i></a>
                            <form method="post" action="{{ route("roles.destroy", $role) }}">
                                @method('DELETE')
                                @csrf
                                <x-button type="submit"><i class="fa-solid fa-fw fa-trash"></i></x-button>
                            </form>
                        </div>
                    </div>
                @empty
                    <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div class="p-6 bg-yellow-100 border-b border-yellow-300 text-yellow-900">
                            {{ __("Roles not found") }}
                        </div>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
</x-app-layout>
