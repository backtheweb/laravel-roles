<?php

use Illuminate\Support\Facades\Route;
use Backtheweb\Roles\Http\Controllers\RolesController;


Route::resource('roles', RolesController::class);
