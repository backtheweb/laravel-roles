## Backtheweb Laravel Roles


## Publish config

```
$ php artisan vendor:publish --provider="Backtheweb\Roles\RolesServiceProvider" --tag="config"
```

## Publish views
```
$ php artisan vendor:publish --provider="Backtheweb\Roles\RolesServiceProvider" --tag="views"
```

## Publish langs
```
$ php artisan vendor:publish --provider="Backtheweb\Roles\RolesServiceProvider" --tag="langs"
```
