<?php

return [
    'ui' => [
        'enable'     => false,
        'prefix'     => 'roles',
        'middleware' => ['web', 'auth'],
    ]
];
