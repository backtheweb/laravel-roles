<?php

return [
    'title'     => 'Roles',
    'deleted'   => 'Role deleted',
    'created'   => 'Role created',
    'updated'   => 'Role updated',
    'role'      => 'Role',
    'name'      => 'Role name',
    'create'    => 'New role',
    'save'      => 'Save role',
];
