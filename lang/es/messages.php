<?php

return [
    'title'     => 'Roles',
    'deleted'   => 'Rol borrado',
    'created'   => 'Rol creado',
    'updated'   => 'Rol actualizado',
    'role'      => 'Rol',
    'name'      => 'Nombre rol',
    'create'    => 'Nuevo rol',
    'save'      => 'Guardar role',
];
