<?php

return [
    'title'     => 'Rols',
    'deleted'   => 'Rol eliminat',
    'created'   => 'Rol creat',
    'updated'   => 'Rol actualitzat',
    'role'      => 'Rol',
    'name'      => 'Nom rol',
    'create'    => 'Nou role',
    'save'      => 'Guardar role',
];
