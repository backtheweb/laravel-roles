<?php

namespace Backtheweb\Roles\Tests;

use Backtheweb\Roles\RolesServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TestCase extends \Orchestra\Testbench\TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
    }

    protected function getPackageProviders($app)
    {
        return [
            RolesServiceProvider::class,
        ];
    }

    /**
     * @param \Illuminate\Foundation\Application $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        config()->set('auth.providers.users.model', \Backtheweb\Roles\Models\User::class);

        include_once __DIR__ . '/../stubs/create_users_table.php.stub';

        (new \CreateUsersTable)->up();
    }
}
