<?php

namespace Backtheweb\Roels\Tests\Unit;

use Backtheweb\Roles\Tests\TestCase;
use Backtheweb\Roles\Models\Role;
use Backtheweb\Roles\Models\User;

class RoleTest extends TestCase
{
    /** @test */
    public function a_role_has_a_name()
    {
        $roleFakeName = 'ROLE_FAKE';

        $role = Role::factory()->create(['name' => $roleFakeName]);
        $this->assertEquals($roleFakeName, $role->name);
    }

    /** @test */
    public function a_user_has_a_role()
    {
        $roleFakeName = 'ROLE_FAKE';

        $user = User::factory()->create();
        $role = Role::factory()->create(['name' => $roleFakeName]);
        $user->roles()->attach($role->getKey());

        $this->assertTrue($user->hasRole($roleFakeName));
    }
}
