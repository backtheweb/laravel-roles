<?php
namespace Backtheweb\Roles\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserRole extends Pivot
{

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'));
    }
}
