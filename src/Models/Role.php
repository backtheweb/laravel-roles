<?php

namespace Backtheweb\Roles\Models;

use Backtheweb\Roles\Database\Factories\RoleFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return RoleFactory::new();
    }
}
