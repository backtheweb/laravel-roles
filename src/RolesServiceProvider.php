<?php

namespace Backtheweb\Roles;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class RolesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'roles');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {

            $this->publishes([__DIR__ . '/../config/config.php' => config_path('roles.php')], 'config');
            $this->publishes([__DIR__ . '/../resources/views' => resource_path('views/vendor/backtheweb/roles')], 'views');
            $this->publishes([__DIR__ . '/../lang' => $this->app->langPath('vendor/roles')], 'langs');

            if (!class_exists('CreateRolesTable')) {

                $this->publishes([
                    __DIR__ . '/../database/migrations/2022_02_12_000000_create_roles_table.php' => database_path('migrations/2022_02_12_000000_create_roles_table.php'),
                    __DIR__ . '/../database/migrations/2022_02_12_000001_create_user_role_table.php' => database_path('migrations/2022_02_12_000001_create_user_role_table.php'),

                ], 'migrations');
            }

        }

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'roles');
        $this->loadTranslationsFrom(__DIR__ . '/../lang', 'roles');

        $this->registerRoutes();
    }

    protected function registerRoutes()
    {
        //Route::model('role', Role::class);

        if (config('roles.ui.enable', false)) {
            Route::group($this->routeConfiguration(), function () {
                $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
            });
        }
    }

    protected function routeConfiguration()
    {
        return [
            'prefix' => config('roles.ui.prefix'),
            'middleware' => config('roles.ui.middleware'),
        ];
    }
}
