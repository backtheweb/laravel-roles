<?php

namespace Backtheweb\Roles\Traits;

use Backtheweb\Roles\Models\Role;

trait Rolable {

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role');
    }

    public function hasRole($role) : bool
    {
        return $this->roles->contains('name', $role);
    }
}
